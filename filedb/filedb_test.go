package filedb

import (
	"io/ioutil"
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

// TestFileDB - testing file database.
func TestFileDB(t *testing.T) {
	content := make([]time.Time, 10)
	for i := 0; i < 10; i++ {
		content[i] = time.Now()
	}
	tmpfile, err := ioutil.TempFile("", "")
	assert.Nil(t, err)
	defer os.Remove(tmpfile.Name())

	filedb, err := New(tmpfile.Name())
	assert.Nil(t, err)
	assert.Nil(t, filedb.Save(content))
	assert.Nil(t, filedb.Close())

	filedb, err = New(tmpfile.Name()) // recreating database using previously recorded events
	assert.Nil(t, err)
	loadedContent, err := filedb.Load()
	assert.Nil(t, err)
	for i := 0; i < 10; i++ {
		assert.Equal(t, loadedContent[i].UnixNano(), content[i].UnixNano(),
			"initial and loaded content should be equal")
	}

	updatedContent := make([]time.Time, 10)
	for i := 0; i < 10; i++ {
		updatedContent[i] = time.Now()
	}
	assert.Nil(t, filedb.Save(updatedContent))
	assert.Nil(t, filedb.Close())

	filedb, err = New(tmpfile.Name()) // recreating database using previously recorded events (updatedContent)
	assert.Nil(t, err)
	loadedContent, err = filedb.Load()
	assert.Nil(t, err)
	for i := 0; i < 10; i++ {
		assert.Equal(t, loadedContent[i].UnixNano(), updatedContent[i].UnixNano(),
			"updated and loaded content should be equal")
	}

}
