// package main - executable file

package main

import (
	"log"
	"os"
	"os/signal"
	"time"

	"gitlab.com/ayzu/recounter/domain"

	"gitlab.com/ayzu/recounter/limiter"

	"gitlab.com/ayzu/recounter/counter"
	"gitlab.com/ayzu/recounter/filedb"
	"gitlab.com/ayzu/recounter/server"
)

const (
	addr           = ""
	port           = "8080"
	backupInterval = time.Millisecond * 500
	countingPeriod = time.Second * 20
	urlPathPattern = "/"
	filename       = "data"
	limit          = 2
	limitOption    = domain.LimitByHeader
)

func main() {
	cntr := counter.New(countingPeriod)

	db, err := filedb.New(filename)
	if err != nil {
		log.Fatalln("failed to connect to the db:", err)
	}

	lmtr := limiter.New(countingPeriod, limit, limitOption)

	srvr, err := server.New(cntr, lmtr, db, addr, port)
	if err != nil {
		log.Fatalln("failed to create the srvr:", err)
	}

	go func() {
		log.Fatalln(srvr.Serve(urlPathPattern))
	}()

	stop := make(chan os.Signal, 1)
	signal.Notify(stop, os.Interrupt)
	makeBackup := time.NewTicker(backupInterval)
	for {
		select {
		case sig := <-stop:
			log.Println("received signal:", sig)
			if err := srvr.Stop(); err != nil {
				log.Println("failed to gracefully shutdown the server:", err)
			} else {
				log.Println("successfully shutdown the server")
			}
		case <-makeBackup.C:
			if err := srvr.Backup(); err != nil {
				log.Println("failed to backup collected events:", err)
			}
		}
	}

}
