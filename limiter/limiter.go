// limiter - implements domain.Limiter.

package limiter

import (
	"log"
	"net"
	"net/http"
	"sync"
	"time"

	"gitlab.com/ayzu/recounter/counter"
	"gitlab.com/ayzu/recounter/domain"
)

// getIPfromRemoteAddr
func getIPfromRemoteAddr(address string) (string, error) {
	ip, _, err := net.SplitHostPort(address)
	return ip, err
}

// Limiter - implements domain.Limiter.
type Limiter struct {
	*sync.Mutex
	counts         map[string]*counter.Counter
	countingPeriod time.Duration
	limit          int
	limitOption    int
}

// ChecksLimits - checks limits.
func (l *Limiter) CheckLimits(req *http.Request) (r domain.Response, err error) {
	l.Lock()
	defer l.Unlock()
	r.Period = l.countingPeriod
	var ip string
	switch l.limitOption {
	case 1:
		ip, err = getIPfromRemoteAddr(req.RemoteAddr)
		if err != nil {
			return r, err
		}
	case 2:
		ip = req.Header.Get("X-Forwarded-For")
	}

	r.IP = ip

	cntr, exists := l.counts[ip]
	if !exists {
		cntr = counter.New(l.countingPeriod)
		l.counts[ip] = cntr
	}

	cntr.CreateEvent()
	receivedRequests := cntr.CountEvents()
	r.NumberOfRequests = receivedRequests
	if receivedRequests <= l.limit {
		r.WithinLimits = true
	}

	log.Println(r)
	return r, nil
}

// New - instantiates a limiter.
func New(duration time.Duration, limit int, limitOption int) *Limiter {
	return &Limiter{
		counts:         make(map[string]*counter.Counter),
		countingPeriod: duration,
		limit:          limit,
		limitOption:    limitOption,
		Mutex:          new(sync.Mutex),
	}
}
