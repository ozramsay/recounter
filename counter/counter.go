// package counter - implements type `Counter` that counts the number of events occurred during the given time interval
// (moving window). `Counter` is not concurrently safe by itself and is supposed to be used with sync.Mutex.

package counter

import (
	"container/list"
	"time"
)

// Counter - counts the number of events occurred during the given time interval (moving window).
// Counter is not concurrently safe by itself and is supposed to be used with sync.Mutex.
type Counter struct {
	*list.List     // represents the stream of events. New events are inserted at the back of the list.
	countingPeriod time.Duration
}

// CreateEvent - adds a new event at the back of the events list.
func (c *Counter) CreateEvent() {
	currentTime := time.Now()
	c.List.PushBack(currentTime)
}

// CountEvents - counts the number of events occurred during the `c.countingPeriod` (moving window).
func (c *Counter) CountEvents() (numberOfEvents int) {
	currentTime := time.Now()
	var next *list.Element
	for event := c.List.Front(); event != nil; event = next {
		next = event.Next()
		eventTime := event.Value.(time.Time)
		// ----A----------B----------now---------
		// -------| collecting period |----------
		// event `A` should be removed as outdated, and event `B` should be preserved.
		if currentTime.Add(-c.countingPeriod).Before(eventTime) { // count the number of unexpired events.
			numberOfEvents++
		} else {
			c.List.Remove(event) // filter events which are older than collecting period
		}
	}
	return numberOfEvents
}

// AddEvents - adds previously recorded events to the counter.
func (c *Counter) AddEvents(events []time.Time) {
	for _, event := range events {
		c.List.PushBack(event)
	}
}

// AllEvents - returns all collected events.
func (c *Counter) AllEvents() []time.Time {
	events := make([]time.Time, 0)
	for event := c.List.Front(); event != nil; event = event.Next() {
		events = append(events, event.Value.(time.Time))
	}
	return events
}

// New - creates a new instance of `Counter`.
func New(countingPeriod time.Duration) *Counter {
	return &Counter{
		List:           list.New(),
		countingPeriod: countingPeriod,
	}
}
